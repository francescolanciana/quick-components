# Quick Components README

Quick Components is a really versatile extension that allows for rapid creation of components (however many files of any type that may include). I've provided a looot of control in choosing how files are created, but have endeavored to make that control more of an opt-in than the default. The default settings can be overwridden using User Settings, Workspace Settings or Workspace Folder Settings.

This extension is garunteed not to overwrite existing components if the name happens to clash.

NOTE: Currently only supports React, however the only thing stopping me from adding support for the other framework/libraries is that I don't know what should go in their template. Once I figure this out adding them in is trivial (all the code is written up already).

## Features

---

A quick component can be created in one of two ways.

1. Right click on the containing folder and select 'Create Component'. You will then be prompted to enter in a component name, after which the component will be created.
2. Cmd + Shift + P (Ctrl + Shift + P on Windows) and type 'Create Component' to select the corresponding option. This will then open a folder selection for selecting the parent folder of the component. Once selected you will then be prompted to enter in a component name, after which the component will be created.

## Extension Settings

---

This extension contributes the following settings:

|                  Name                   |           Type            |    Default     | Description                                                                                                                                                                                                                            |
| :-------------------------------------: | :-----------------------: | :------------: | :------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
|   **`quickcomponents.componentType`**   |        `{String}`         |    `React`     | Specifies what type of component you would like to create. E.g. React, Angular, ect...                                                                                                                                                 |
|  **`quickcomponents.componentFiles`**   | `{Array<String\|Object>}` | `["Defaults"]` | Allows fine grained control of the files generated per component and the content inside those files.                                                                                                                                   |
| **`quickcomponents.customFileConfigs`** |    `{Object<Object>}`     |      `{}`      | Allows custom file configs to be created and reused at any point (in quickcomponents.componentFiles).                                                                                                                                  |
|    **`quickcomponents.smartLinks`**     |        `{Boolean}`        |     `true`     | Specifies whether smart guesses should be made as to which files make use (form links) with other files.                                                                                                                               |
| **`quickcomponents.componentFolders`**  |     `{Array<String>}`     |     `null`     | Generates an empty folder for each element in the array (same name as the string). These folders are siblings of the component files.                                                                                                  |
| **`quickcomponents.containingFolder`**  |        `{Boolean}`        |     `true`     | Set to `false` to generate the component files within the selected folder and not in a sub-folder of their own.                                                                                                                        |
|     **`quickcomponents.openMain`**      |        `{Boolean}`        |    `false`     | Set to `true` to automatically open the main component file. The default file to be opened depends on the componentType selected (see detailed description below), however this can be changed using `quickcomponents.componentFiles`. |
|   **`quickcomponents.applyTemplate`**   |    `{String\|Object}`     |     `null`     | Supply a path to a template folder/file you created to create new components with your own custom template.<br>⚠️ If a value is provided for this setting all other settings will be overwrited.                                       |

<br/>

### `quickcomponents.componentType`

---

Currently only React components are supported, however support is on the way for Angular, Vue, Ember (Feel free to suggest more Component Types). I've created the core extension logic in a way that adding support for new component types only requires changing some configs and adding a template, no code needs to be written.

```json
"quickcomponents.componentType": "React"
```

<br />

### `quickcomponents.componentFiles`

---

Accepts an array which can contain both **strings** and **objects**.

-   **Strings** allow for easy selection of files to generate which use the default settings. Providing a String is equivalent to using `componentFile.fileType` if using an Object instead of a String.
-   **Objects** are file configs that give fine grained control of the file extension and contents. File configs have three important properties: `fileType`, `fileOptions`, and `templateOptions`.

The "Default" fileType is a special file type that actually represents multiple files. Which files it represents differs depending on the componentType. The defaults for each componentType are as follows:
| Component Type | Defaults |
|:--------------:|:-----------------------------------------------------:|
|`"React"` |`"Component"`, `"Stylesheet"` |
|`"Angular"` |`"View"`, `"Controller"`, `"Service"`, `"Stylesheet"` |
|`"Vue"` |`"Component"`, `"Stylesheet"` |

Please keep in mind that all `fileOptions` and `templateOptions` keys are case sensitive, (i.e. you can't give preprocessor as a templateOption key, it needs to be preProcessor).

#### `componentFile.fileType`

Specifies the file type to be generated. Each UI library/framework employs different concepts to structure code, for example Angular has the concept of a directive while React does not. Therefore there are a large number of possible file types. If you specify a file type that doesn't correspond to the UI library/framework you selected in `quickcomponents.componentType` then it will not be generated.

|      Name      |    Type    |                                                    Accepted values                                                     |
| :------------: | :--------: | :--------------------------------------------------------------------------------------------------------------------: |
| **`fileType`** | `{String}` | `"Component"` \| `"Controller"` \| `"Service"` \| `"View"` \| `"Directive"` \| `"Stylesheet"` \| `"Test"` \| `"Blank"` |

#### `componentFile.fileOptions`

This is where you can customize the file itself. These options do not relate to the fileType (they are common across all file types).

**`fileExtension`**  
You have complete freedom with this one. Unsafe file extensions will be sanitized by removing directory paths and invalid characters. The default depends on the `componentType` and `fileType` specified.<br/><br/>
Type: `String`

| Component Type |   File Type    |      Default      |
| :------------: | :------------: | :---------------: |
|   `"React"`    | `"Component"`  |      `"jsx"`      |
|  `"Angular"`   |    `"View"`    |     `"html"`      |
|  `"Angular"`   | `"Controller"` |      `"js"`       |
|  `"Angular"`   | `"Component"`  | `"components.js"` |
|  `"Angular"`   |  `"Service"`   |      `"js"`       |
|  `"Angular"`   | `"Directive"`  |      `"js"`       |
|  `"Angular"`   |   `"Filter"`   |      `"js"`       |
| `"All types"`  | `"Stylesheet"` |      `"css"`      |
|   `"React"`    |    `"Test"`    |    `"test.js"`    |
|  `"Angular"`   |    `"Test"`    |    `"spec.js"`    |
| `"All types"`  |   `"Blank"`    |      `"js"`       |

**`main`**  
If `quickcomponents.openMain` is set to true than the **main** file will be automatically opened after the component is generated. If multiple component files are generated then the file corresponding to the last entry in the array shall be opened.
Type: `Boolean`  
Default: `true`

**`fileName`**  
Allows you to customize the name of your files in any way you would like. The componentName placeholder can be used here. Unsafe file names will be sanitized by removing directory paths and invalid characters.
Type: `String`  
Default: `[componentName]`

<br />

#### `componentFile.templateOptions`

For a given type of file you can customize the template applied via these options. These options are specific to a given file type, and often a component type as well. For example `propTypes` only applies when the componentType is React and the fileType is Component. If a component type is not specified than it applies for all component types.

**`componentName` (componentType: React -- File Type: Component)**  
Allows you to customize the name of the component in the generated file. In this way the component name and file name can differ but by default they do not (I'm not really sure why you would want them to either).  
Type: `String`  
Default: `[componentName]`

**`className` (componentType: React -- File Type: Component)**  
Allows you to customize the css class name that is assigned to the outermost html element in the component. With this you can either replace the class name completely or augment the default by appending text to the class name placeholder.  
Type: `String`  
Default: `[componentClassName]`

**`propTypes` (componentType: React -- File Type: Component)**  
If `true` PropTypes will be imported and the corresponding code to use PropTypes will be generated.  
Type: `Boolean`  
Default: `false`

**`classComponent` (componentType: React -- File Type: Component)**  
If `true` this will generate a class component instead of a functional component.  
Type: `Boolean`  
Default: `false`

**`commonLifeCycleMethods` (componentType: React -- File Type: Component)**  
Include common lifecycle methods in the generated file.  
Type: `Boolean`  
Default: `false`

**`uncommonLifeCycleMethods` (componentType: React -- File Type: Component)**  
Include uncommon lifecycle methods in the generated file.  
Type: `Boolean`  
Default: `false`

**`preProcessor` (File Type: Style)**  
Allows you to specify if a css preprocessor is being used (and if so which). This will alter the template used as in some cases the preprocessor syntax will be different to that of css (e.g. Stylus)  
Type: `String|Object`  
Default: `"none"`
Options: `"sass"`, `"scss"`, `"less"`, `"stylus"`

Currently only Jest is supported for test files however this will be updated very shortly using the `testFramework` config option.

<br/>

An example array for `quickcomponents.componentFiles` is as follows:

```json
"quickcomponents.componentFiles": [
    {
        "fileType": "Component",
        "fileOptions": {
            "fileExtension": "js",
            "fileName": "[componentName]",
            "main": true,
        },
        "templateOptions" : {
            "className": "[componentClassName]-container",
            "propTypes": true,
            "commonLifeCycleMethods": false,
            "uncommonLifeCycleMethods": false
        }
    },
    "Stylesheet"
]
```

<br />

### `quickcomponents.customFileConfigs`

---

Accepts an object where each key is a file config name, and the corresponding value is the file config options. The supplied config options overwrite the default options for a given component and file type, so you do not need to specify all the fileOptions or templateOptions, only those that differ from the default.

```json
"quickcomponents.customFileConfigs": {
    "ComponentWithProps": {
        "fileType": "Component",
        "templateOptions" : {
            "propTypes": true
        }
    }
}
```

This custom file can then be used in `quickcomponents.componentFiles` like so:

```json
"quickcomponents.componentFiles": ["ComponentWithProps"]
```

<br />

### `quickcomponents.smartLinks`

---

This extension can make reasonable assumptions as to which files make use of other files (e.g. A component will usually import a stylesheet, and the stylesheet will therefore want to specify the styling for that component). If this is false files will be created completely independently of all other files. This is handy if the guesses that this extension makes are incorrect for your use case.

Type: `Boolean`  
Default: `true`

```json
"quickcomponents.componentFolders": `true`
```

<br />

### `quickcomponents.componentFolders`

---

Accepts an array of **strings**. The componentName placeholder can be used here. An example array for `quickcomponents.componentFiles` is as follows:

```json
"quickcomponents.componentFolders": ["[componentName]Helpers"]
```

<br />

### `quickcomponents.applyTemplate` (TO BE IMPLEMENTED VERY SOON)

---

Allows for you to define a completely custom template which will be instantiated any time you create a new component with this extension. The path must point to a folder that has one or more files. You can also define a mapping for as many strings as you would like using `stringMap`. Whenever a string matching a key is encountered, it is replaced with the corresponding value. You can use placeholders for the values (which is pretty much the only reason you would want to use this option). It is important to note that if you use this option all other config settings will be completely ignored; this is a handy escape hatch. An example array for `quickcomponents.applyTemplate` is as follows:

```json
"quickcomponents.applyTemplate": {
    "path": "",
    "stringMap": {
        "componentname": "[componentName]",
        "export": "Banana"
    }
}
```

<br />

## Placeholders

### `[componentName]`

Type: `String`  
Default: The name supplied by you when creating the component.

### `[componentClassName]`

Type: `String`  
Default: This is a transformed version of `[componentName]`, whereby the components name is converted to lowercase, and a hyphen (-) is inserted before every capital letter, (e.g. ARandomExample -> a-random-example). In the case that an acronym is used, (by having multiple uppercase letters) then the acronym will be preserved, (e.g. MyVSCodeExample -> my-vs-code-example).

## Known Issues

This extension has pretty extensive features with plenty of permutations so it's very possible I missed something. I have not yet implemented a test suite as I was essentially writing breaking changes every other week (don't worry, that isn't happening anymore). Let me know if you find something though and I'll credit you in the documentation :)
