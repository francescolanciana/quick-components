const sanitize = require("sanitize-filename");
const fs = require('fs');
const Mustache = require('mustache');

const { convertToClass } = require('./helpers.js');

class Module {
    constructor({ fileType, fileOptions, templateOptions }) {
        this.fileType = fileType;
        this.fileOptions = fileOptions;
        this.templateOptions = templateOptions;
    }

    applyUserOptions({ fileOptions, templateOptions }) {
        this.fileOptions = Object.assign({}, this.fileOptions, fileOptions);
        this.templateOptions = Object.assign({}, this.templateOptions, templateOptions);
    }

    sanitizeUserFileOptions() {
        return Object.keys(this.fileOptions).reduce((sanitizedOptions, key) => {
            const value = this.fileOptions[key];

            if (key === "fileExtension" || key === "fileName") sanitizedOptions[key] = sanitize(value);
            else sanitizedOptions[key] = value;

            return sanitizedOptions;
        }, {})
    }

    setTemplatePath(templatePath) {
        this.templatePath = templatePath;
    }

    setId(id) {
        this.id = id;
    }

    fillInPlaceHolders(componentName) {
        this.fileOptions = this.replaceAllPlaceholders(this.fileOptions, componentName);
        this.templateOptions = this.replaceAllPlaceholders(this.templateOptions, componentName);
    }

    replaceAllPlaceholders(object, componentName) {
        const newObject = Object.assign({}, object);
        const className = convertToClass(componentName);

        Object.keys(newObject).forEach((key) => {
            if (!(typeof newObject[key] === "string")) return;

            newObject[key] = newObject[key].replace("[componentName]", componentName);
            newObject[key] = newObject[key].replace("[componentClassName]", className);
        });

        return newObject;
    }

    toFile(fileFullPath, moduleDependencies) {
        return new Promise((resolve, reject) => {
            fs.readFile(this.templatePath, (err, contents) => {
                if (err) reject(new Error(`The '${this.fileType}' template file could not be loaded. Error code: ${err.code}`));
    
                const filledTemplate = Mustache.render(contents.toString(), Object.assign({}, this.templateOptions, moduleDependencies));
                
                fs.writeFile(fileFullPath, filledTemplate, (err) => {
                    if (err) {
                        reject(new Error(`The '${this.fileType}' file could not be created. Error code: ${err.code}`));
                    } else {
                        resolve();
                    }
                });
            });
        });
    }

    getFileNameWithExtension() {
        return `${this.fileOptions.fileName}.${this.fileOptions.fileExtension}`;
    }
}

module.exports = { Module };