const vscode = require('vscode');
const path = require('path');
const sanitize = require("sanitize-filename");
const toml = require('toml');
const fs = require('fs');

const { ModuleFactory } = require('./moduleFactory');
const { capitalizeFirstLetter, normalizeFileConfigs } = require("./helpers.js");
const { returnExistentComponentDir, FileFactory } = require('./fileGenerators.js');

const defaultFileTypes = toml.parse(fs.readFileSync(path.resolve(__dirname, 'Config/defaultFileTypes.toml'), 'utf-8'));

if (!Array.prototype.last){
    Array.prototype.last = function(){
        return this[this.length - 1];
    };
};

function activate(context) {
    let disposable = vscode.commands.registerCommand('extension.createComponent', (providedDirectory) => {
        const configuration = vscode.workspace.getConfiguration('quickcomponents');

        getUserInputs(providedDirectory)
        .then(([ selectedFolders, componentName ]) => validateUsersInputs(selectedFolders, componentName))
        .then(({ parentDirectoryPath, componentName }) => createComponent(parentDirectoryPath, componentName, configuration))
        .then((successMessage) => {
            const message = vscode.window.setStatusBarMessage(successMessage);

            setTimeout(() => message.dispose(), 4000);
        })
        .catch((err) => vscode.window.showErrorMessage(err.message));
    });

    context.subscriptions.push(disposable);
}


/* If the user used the explorer context menu then simply extract the path provided,
   however if the user used the command palette then a parent folder still needs to be 
   selected. The user will then be prompted for the components name. */
function getUserInputs(providedDirectory) {
    const openDialogOptions = { canSelectFiles: false, canSelectFolders: true, canSelectMany: false };

    /* vscode folder selection returns an array even if one folder is selected so to make extracting the 
       path easier we put the provided path in an array (if there was a path provided) */
    const getParentDirectory = providedDirectory ? Promise.resolve([providedDirectory]) : vscode.window.showOpenDialog(openDialogOptions);
    const getComponentsName = getParentDirectory.then(() => vscode.window.showInputBox({ prompt: 'Enter your components name' }));

    return Promise.all([getParentDirectory, getComponentsName]);
}


function validateUsersInputs(selectedFolders, componentName) {
    if (componentName) {
        const parentDirectoryPath = selectedFolders[0].fsPath;

        if (/^[^A-Z]/.test(componentName)) {
            componentName = capitalizeFirstLetter(componentName);
            vscode.window.showWarningMessage("The component name's initial letter was capitalized to comply with React component standards");
        }

        componentName = sanitize(componentName);

        return Promise.resolve({ parentDirectoryPath, componentName });
    } else {
        return Promise.reject(new Error('No component name was provided.'));
    }
}


function createComponent(parentDirectoryPath, componentName, configuration) {
    const moduleFactory = new ModuleFactory(componentName);
    const componentDirPath = path.resolve(parentDirectoryPath, componentName);

    // Manipulate configs so they can be handles in the same way
    const fileConfigsExpandedDefaults = expandDefaults(configuration.componentFiles, configuration.componentType);
    const fileConfigsReplacedCustoms = replaceCustoms(fileConfigsExpandedDefaults, configuration.customFileConfigs);
    const normalizedFileConfigs = normalizeFileConfigs(fileConfigsReplacedCustoms, configuration.componentType);
    const fileConfigsWithFileType = normalizedFileConfigs.filter((fileConfig) => fileConfig.fileType);
    let moduleUniqueId = 1;

    // If an invalid config is provided the corresponding module will be undefined
    const modules = fileConfigsWithFileType
        .map((fileConfig) => {
            const module = moduleFactory.createModule(fileConfig, moduleUniqueId, handleInvalidFileType, handleInvalidDepValue, handleMissingHardDepValue);
            moduleUniqueId++;

            return module;
        })
        .filter((module) => module);
   
    const reducedModules = filterDuplicateModules(modules);

    return Promise.resolve()
        .then(() => returnExistentComponentDir(componentDirPath, configuration.containingFolder))
        .then((parentDir) => {
            const fileFactory = new FileFactory(parentDir);
            const createFiles = fileFactory.createFiles(reducedModules, configuration.smartLinks);
            const createFolders = fileFactory.createFolders(configuration.componentFolders);

            return Promise.all([createFiles, createFolders]);
        })
        .then(() => openMainFileIfApplicable(reducedModules, componentDirPath))
        .then(() => Promise.resolve(componentName + ' was created successfully'))
        .catch((err) => Promise.reject(err));
}

function replaceCustoms(fileConfigs, customFileConfigs) {
    return fileConfigs.map((fileConfig) => {
        if (typeof fileConfig === "string" && customFileConfigs[fileConfig]) {
            return customFileConfigs[fileConfig];
        } else {
            return fileConfig;
        }
    })
}

function expandDefaults(fileConfigs, componentType) {
    const expandedFileConfigs = [...fileConfigs];

    const defaultFileTypeIndexes = fileConfigs.reduce((indexes, fileConfig, i) => {
        if (typeof fileConfig === "string" && fileConfig.toLowerCase() === "defaults") return [...indexes, i];
        else return indexes;
    }, []);

    defaultFileTypeIndexes.forEach((index) => {
        expandedFileConfigs.splice(index, 1, ...getDefaults(componentType.toLowerCase()));
    });

    return expandedFileConfigs;
}


function getDefaults(componentType) {
    return defaultFileTypes[componentType] ? defaultFileTypes[componentType].files : [];
}


function handleInvalidFileType(fileOptions) {
    vscode.window.showWarningMessage(`The fileType ${fileOptions.fileType} doesn't match any of the provided types or custom types as defined by you, therefore it was not created`);
}

function handleInvalidDepValue(fileType, dependencies) {
    const dependencyString = dependencies.map((dependency) => dependency.property.last()).join(', ');
    vscode.window.showWarningMessage(`The default options for the file type '${fileType}' could not be found as the values defined for one or more of it's dependencies (${dependencyString}) are not valid.`);
}

function handleMissingHardDepValue(fileType, dependencies) {
    const dependencyString = dependencies
        .filter((dependency) => dependency.hard)
        .map((dependency) => dependency.property.last())
        .join(', ');
    vscode.window.showWarningMessage(`The default options for the file type '${fileType}' could not be found as the there was no values provided for one or more of it's hard dependencies: (${dependencyString}).`);
}


/* We can't garuntee that their is a main file even though it's set in defaultOptions as this can be
   overwrited by the user */
function openMainFileIfApplicable(modules, componentDirPath) {
    let mainFilePath;

    for (let i = modules.length - 1; i >= 0; i--) {
        const { main, fileName, fileExtension } = modules[i].fileOptions;

        if (main) {
            mainFilePath = path.resolve(componentDirPath, `${fileName}.${fileExtension}`);
            break;
        }
    }

    if (mainFilePath) {
        return vscode.workspace.openTextDocument(mainFilePath)
            .then((doc) => vscode.window.showTextDocument(doc));
    } else {
        vscode.window.showWarningMessage("Couldn't open the main component file because no file was specified as the main file");
        return Promise.resolve();
    }

}

/* It is possible to create multiple files of any given type for a component, however if
    they don't at least differ by name then there really isn't a nice way to interpret what the
    user meant (i.e. what should they now be named since we can't have clashing file names). It's 
    more probable that it was a mistake and should be removed (still report the problem though). */
function filterDuplicateModules(modules) {
    const componentIds = new Set();
    const duplicateComponentTypes = new Set();

    const filteredModules = modules.filter((module) => {
        const componentFileId = module.fileType + ":" + module.fileOptions.fileName;

        if (!componentIds.has(componentFileId)) {
            componentIds.add(componentFileId);
            return true;
        } else {
            duplicateComponentTypes.add(module.fileType);
            return false;
        }
    });

    if (duplicateComponentTypes.size !== 0) {

        const removedFileTypes = [...duplicateComponentTypes].reduce((removedFileTypes, type, i) => {
            return i > 0 ? removedFileTypes + ", " + type : removedFileTypes + type;
        }, "");
    
        vscode.window.showWarningMessage(`The duplicate files of the following type were not created: ${removedFileTypes}. Duplicated file types must at least differ by name to be created.`);
    }

    return filteredModules;
}


exports.activate = activate;

// // this method is called when your extension is deactivated
// function deactivate() {
// }
// exports.deactivate = deactivate;