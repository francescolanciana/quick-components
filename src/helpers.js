function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function convertToClass(componentName) {
    let currentChunk = "";
    let classNameChunks = [];
    
    for (let i = 0; i < componentName.length; i++) {
      let currentChar = componentName[i];
      let nextChar = componentName[i+1];
      
      if (!nextChar) {
        classNameChunks.push(currentChunk + currentChar);
        break;
      }
      
      let currentCharCase = currentChar.search(/[A-Z]/);
      let nextCharCase = nextChar.search(/[A-Z]/);
      
      const LOWERCASE = -1;
      const UPPERCASE = 0;
      if (currentCharCase === nextCharCase) {
        currentChunk += currentChar;
        continue;
      }
      
      if (currentCharCase === UPPERCASE && nextCharCase === LOWERCASE) {
        if (currentChunk) classNameChunks.push(currentChunk);
        currentChunk = currentChar;
        continue;
      }
      
      if (currentCharCase === LOWERCASE && nextCharCase === UPPERCASE) {
        currentChunk += currentChar;
        classNameChunks.push(currentChunk);
        currentChunk = "";
        continue;
      }
    }

    const className = classNameChunks
        .map((word) => word.toLowerCase())
        .join('-');

    return className;
}

function normalizeFileConfigs(fileConfigs, componentType) {
    return fileConfigs.map((fileConfig) => {
        if (typeof fileConfig === "string") {
            return { componentType, fileType: fileConfig };
        } else {
            return Object.assign({ componentType }, fileConfig);
        }
    })
}

module.exports = { capitalizeFirstLetter, convertToClass, normalizeFileConfigs };