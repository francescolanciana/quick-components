const vscode = require('vscode');
const fs = require('fs');
const path = require('path');
const toml = require('toml');

const fileTypeDependencies = toml.parse(fs.readFileSync(path.resolve(__dirname, 'Config/fileTypeDependencies.toml'), 'utf-8'));


/* Returns the folder that we can write files to, thereby abstracting away whether we have 
   created a folder to house the component files or not. Also covers every damn edge case
   under the sun. */
function returnExistentComponentDir(directory, containingFolder) {  
    return new Promise((resolve, reject) => {
        fs.readdir(path.dirname(directory), (err, files) => {
            const matchingFiles = files.filter((file) => {
                return file.split(".")[0].toLowerCase() === path.basename(directory).toLowerCase();
            })

            if (matchingFiles.length === 1) {
                const fileFullPath = path.resolve(path.dirname(directory), matchingFiles[0]);

                // We need to check if this file is a file or folder
                fs.stat(fileFullPath, (err, stats) => {
                    if (stats.isDirectory()) {
                        /* If there is a folder with the same name as the component to be we could still go
                           ahead, but only if it is empty */
                        if (containingFolder) {
                            fs.readdir(fileFullPath, (err, files) => {
                                if (err) reject(new Error('The component directory already existed but there was an error when performing checks against the files inside. Error code: ' + err.code));
                                // In the case we had one file, which turned out to be an empty folder, this should be equivalent to our directory argument.
                                else if (files.length === 0) resolve(directory);
                                else reject(new Error('The component directory already exists and contains files. To avoid any unwanted overwrites the process was stopped.'));
                            });
                        } else {
                            /* There was already a component folder but we only want to create files which is 
                               technically allowed (but silly). Throw a warning and get on out of here */
                               vscode.window.showWarningMessage("What you are doing is technically OK but you really shouldn't have a folder and files with the same name in the same directory.");
                               resolve(path.dirname(directory));
                        }
                    } else {
                        /* We know the file isn't a folder, so now we have to check if our new component goes in 
                        a folder or not. If it does it's technically fine (but silly), if not it's time to throw an error */
                        if (containingFolder) {
                            vscode.window.showWarningMessage("What you are doing is technically OK but you really shouldn't have a folder and file with the same name in the same directory.");
                            fs.mkdir(directory, () => resolve(directory));
                        } else {
                            reject(new Error('The parent directory already contains a file that has the same name as your component to be.'));
                        }
                    }                    
                });
            }
            else if (matchingFiles.length > 1 && !containingFolder) {
                // We cant have folders with the same name so we know there is at least one file
                reject(new Error('The parent folder already contained file/s whose names clash with the component to be.'));
            } 
            else if (matchingFiles.length === 0) {
                // No files/folders were found so we are safe to go ahead
                if (containingFolder) {
                    fs.mkdir(directory, () => resolve(directory));
                } else {
                    resolve(path.dirname(directory));
                }
            }
            else {
                /* We are left with the edge case whereby there are multiple files which could or could not
                   be folders and the user wants to make a folder for their component files. This is quite
                   the edge case, and the only way to sort it out is check if any of the matching files are
                   folders. If they are we can throw an error. If they aren't the component can be created
                   although I really don't know why anyone would do this so we can chuck a warning */
                return Promise.all(
                    matchingFiles.map((fileName) => {
                        return new Promise((resolve, reject) => {
                            const fileFullPath = path.resolve(path.dirname(directory), fileName);

                            fs.stat(fileFullPath, (err, stats) => {
                                if (err) reject(new Error('An error occured while checking if the files whose names match your components where actually directories. Error code: ' + err.code));

                                if (stats.isDirectory()) {
                                    reject(new Error('The folder with the same name as your component to be already exists! Also consider refactoring to avoid files and folders with the same name at the same level.'));
                                } else {
                                    resolve();
                                }
                            });
                        });
                    })
                )
                .then(() => {
                    vscode.window.showWarningMessage("What you are doing is technically OK but you really shouldn't have a folder and files with the same name in the same directory.");
                    fs.mkdir(directory, () => resolve(directory));
                })
                .catch((err) => reject(err));
            }
        });
    });
}


class FileFactory {
    constructor(parentDir) {
        this.parentDir = parentDir;
        this.recordedModuleLinks = {};
    }

    createFiles(modules, smartLinks) {
        const groupedModules = this.groupModulesByFileType(modules);

        return Promise.all(
            Object.keys(groupedModules).map((moduleType) => {

                // Lookup what file types the module group we are processing depends on
                const moduleGroup = groupedModules[moduleType];
                const moduleGroupFileTypeDependencies = fileTypeDependencies[moduleType];
    
                // Extract groups of modules whose types correspond to the previous lookup
                const dependenciesModuleGroups = this.extractModuleGroupsByFileType(moduleGroupFileTypeDependencies.fileTypes, groupedModules);
                
                return Promise.all(
                    // Process each file in the current group of modules. Look at CHANGELOG 2.0.0 for in depth reasoning.
                    moduleGroup.map((module) => {
                        const moduleDependencies = smartLinks ? this.linkModules(module, dependenciesModuleGroups) : {};
                        const fileFullPath = path.resolve(this.parentDir, `${module.getFileNameWithExtension()}`);
        
                        return module.toFile(fileFullPath, moduleDependencies);
                    })
                );
            })
        );
    }


    linkModules(module, dependenciesModuleGroups) {
        // Check if any other modules have previously used this module as a dependency (and therefore formed a link)
        const linkedModules = this.recordedModuleLinks[module.id];
        const moduleDependencies = {};

        /* We need to go through each module group corresponding to a file type this module depends on, linking one module from each
        group with the current module. Once these links have been formed and recorded the module will be outputted to a file. */
        Object.keys(dependenciesModuleGroups).forEach((fileType) => {
            const modules = dependenciesModuleGroups[fileType];

            /* Check if any module in this group has already formed a link with the module being processed. If a linked module 
            exists we want to use that, otherwise form a link with the first module in the group */
            const linkedModule = linkedModules && modules.find(module => linkedModules.includes(module.id));
            const moduleDependency = linkedModule || modules[0];

            if (moduleDependency) {
                moduleDependencies[moduleDependency.fileType.toLowerCase()] = moduleDependency;

                // While processing the current module group we want to avoid creating more than one link to any given module (therefore we remove it) 
                const moduleIndex = dependenciesModuleGroups[fileType].findIndex((module) => module.id === moduleDependency.id);
                dependenciesModuleGroups[fileType].splice(moduleIndex, 1);

                // Record the newly created link for when we process the module that is currently treated as a dependency
                if (this.recordedModuleLinks[moduleDependency.id]) this.recordedModuleLinks[moduleDependency.id].push(module.id);
                else this.recordedModuleLinks[moduleDependency.id] = [module.id];
            }
        });

        return moduleDependencies;
    }

    extractModuleGroupsByFileType(fileTypes, groupedModules) {
        const moduleGroups = {};

        fileTypes.forEach((fileType) => {
            if (groupedModules[fileType]) {
                moduleGroups[fileType] = [...groupedModules[fileType]];
            }
        });

        return moduleGroups;
    }

    groupModulesByFileType(modules) {
        return modules.reduce((groupedModules, module) => {
            const fileType = module.fileType.toLowerCase();

            if (!groupedModules[fileType]) groupedModules[fileType] = [module];
            else groupedModules[fileType].push(module);

            return groupedModules;
        }, {});
    }
    

    createFolders(folders) {
        if (!folders) return Promise.resolve();

        return Promise.all(
            folders.map((componentFolder) => {
                return new Promise((resolve, reject) => {
                    const folderFullPath = this.parentDir + path.sep + componentFolder;
                    fs.mkdir(folderFullPath, (err) => {
                        if (err) {
                            if (err.code == 'EEXIST') reject('The folder you are trying to create for your component already exists. (Consider using the quickExtension.containingFolder setting to put all component files in a new folder.');
                            reject(err);
                        }
    
                        resolve();
                    });
                });
            })
        );
    }
}

module.exports = { returnExistentComponentDir, FileFactory };