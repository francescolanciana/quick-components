const fs = require('fs');
const path = require('path');
const toml = require('toml');

const { Module } = require('./module.js');
const { capitalizeFirstLetter } = require("./helpers.js");

const defaultFileOptionDependencies = toml.parse(fs.readFileSync(path.resolve(__dirname, 'Config/defaultFileOptionDependencies.toml'), 'utf-8'));
const defaultOptions = toml.parse(fs.readFileSync(path.resolve(__dirname, 'Config/defaultFileOptions.toml'), 'utf-8'));

class ModuleFactory {
    constructor(componentName) {
        this.componentName = componentName;
    }

    createModule(fileOptions, moduleUniqueId, onInvalidFileType, onInvalidDepValue, onMissingHardDepValue) {
        if (!fileOptions || !fileOptions.fileType) return;

        const fileType = fileOptions.fileType.toLowerCase();
        const fileTypeCapitalized = capitalizeFirstLetter(fileType);
        const dependencies = defaultFileOptionDependencies[fileType];

        if (!dependencies) {
            if (onInvalidFileType) onInvalidFileType(fileOptions);
            return;
        }

        const dependencyValues = this.extractDependenciesValues(fileOptions, dependencies, fileType);

        if (!dependencyValues) {
            if (onMissingHardDepValue) onMissingHardDepValue(fileType, dependencies);
            return;
        }

        const templatePath = path.resolve(__dirname, `Templates/${fileTypeCapitalized}/${dependencyValues.join(".")}.mst`);

        const completePropertyChain = [fileType, ...dependencyValues];
        const defaultFileOptions = this.walkObjectProperties(defaultOptions, completePropertyChain);

        if (!defaultFileOptions) {
            if (onInvalidDepValue) onInvalidDepValue(fileType, dependencies);
            return;
        }

        const module = new Module(defaultFileOptions);
        module.applyUserOptions(fileOptions);
        module.setTemplatePath(templatePath);
        module.fillInPlaceHolders(this.componentName);
        module.sanitizeUserFileOptions();
        module.setId(moduleUniqueId);

        return module;
    }

    extractDependenciesValues(fileOptions, dependencies, fileType) {
        const dependencyValues = [];

        for (let i = 0; i < dependencies.length; i++) {
            let { property, hard } = dependencies[i];

            let dependencyValue = this.walkObjectProperties(fileOptions, property);

            if (dependencyValue) dependencyValues.push(dependencyValue.toLowerCase());
            else if (!hard) {
                const defaultDep = this.getDefaultSoftDependencyValue([fileType, ...dependencyValues]);
                dependencyValues.push(defaultDep);
            }
            else {
                /* Throw error (If there is a hard requirement on this value being provided then there is no 
                default set of options to map to using the none keyword, therefore this will fail) */
                return;
            }
        }

        return dependencyValues;
    }

    getDefaultSoftDependencyValue(dependencyValues) {
        const fileGroup = this.walkObjectProperties(defaultOptions, dependencyValues);

        return fileGroup.default;
    }

    walkObjectProperties(object, propertyChain) {
        let currentValue = object;

        for (let i = 0; i < propertyChain.length; i++) {
            let value = currentValue[propertyChain[i]];

            if (!value) return;
            else currentValue = value;
        }

        return currentValue;
    }
}





module.exports = { ModuleFactory };
