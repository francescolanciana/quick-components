# Change Log
All notable changes to the "quick-folders" extension will be documented in this file.

## 1.0.0

Initial release of Quick Components. This contains support for React components, Sass, and css. 

## 1.0.1

README.md written up.

## 1.0.2

Fixed windows specific bug whereby the path property of file object returned by vscode was incorrect. Now using the fsPath property. Re-imagined the config and wrote up the corresponding spec in the README. Still need to implement...

## 1.1.0

Massive reimplementation. Added a number of new config options such as:
* Specifying if you want your components in a new folder or the current one
* Auto-opening the main file (marked in fileConfig) after the component is created
* Ability to create empty folders to go along with your new component

Along with these config changes you no longer had to specify the type of framework/library for each file, instead you specify it in the option `quickcomponents.componentType`. This greatly simplified the config and code. 
The README was also massively updated to allow users to follow along.

Note: Not all these new options were actually hooked up.

## 1.2.0
At this point all options except for applyTemplate were hooked up and working perfectly. However the way it had been coded made extending this past React components very challenging.

## 1.2.1
Patched a bug that caused stylesheets to break if you didn't specify a preprocessor

## 2.0.0
Completely refactored the code base to allow components to be created for any library/framework. The extension can now hypothetically be extended out to just about anything without touching the codebase, instead only TOML config files need to be touched, and obviously the new template needs to be added. All config options have now been hooked up as well. The following options have been added/edited:
* `quickcomponents.fileConfigs` now accepts the string `"Defaults"`. This is done so that switching to a different framework doesn't necessarily involve you changing the default componentFiles as well, (this would be annoying). It also prevents the situation whereby the default componentFiles is ["Component", "Stylesheet"] but a library/framework comes out that doesn't have the concept of a component file. Then if the user was to change the componentType to that new library/framework it would break since there is no template for a "Component" for the new componentType (it actually wouldn't break, the file just wouldn't be generated).
* `quickcomponents.customFileConfigs` allows users to store complex custom file configurations permanently and use them in `quickcomponents.fileConfigs` by simply referencing the key.
* Files are now linked where it makes sense, so that the generated files can actually reference each other and have their content be affected by other files. This behaviour can be turned off via `quickcomponents.smartLinks`.

The smart links implementation is a little tricky so here is an depth explanation:  
Every module can have dependencies (i.e. A Stylesheet depends on a component to know what class name it should be defining styles for). We then take the module in question, and one module corresponding to each of its dependencies, and using these objects we can properly instantiate a template file with the right values. We can't however just pick its dependencies at random. Consider the example where we have a component and two stylesheets. To instantiate the component module we selected a stylesheet module we want to import. This works great, and we now want to instantiate the stylesheets which in turn depend on components. So we select the first stylesheet and grab a component module, however we have to make sure this was the same component that imported the stylesheet in the first place to not get mismatches. So we need to keep a track of files that have already been used by other files (i.e. the links formed between files).